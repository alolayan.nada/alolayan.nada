# Hello! 
I'm Nada, a Full-stack software developer with background in computer science and PhD in information science. 
Welcome to my second repository in Gitlab,contact me for more information on my first repository on GitHub.<br />
**Status:** 🎓Postdoc,Software Engineer 

Feel free to contact me for more inofrmation!
[![](https://img.shields.io/badge/-GMAIL-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:alolayan.nada@gmail.com) 



# GitHub stats
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=oneatatimet&layout=compact&theme=merko) 

# Technologies and tools
![](https://img.shields.io/badge/OS-Mac-yellow?style=plasticstyle=flat&logo=apple) ![](https://img.shields.io/badge/Editor-VisualStudio-yellow?style=plasticstyle=flat&logo=visual-studio-code) ![](https://img.shields.io/badge/Code-JavaScript-yellow?style=plasticstyle=flat&logo=javascript) ![](https://img.shields.io/badge/Code-Node.js-yellow?style=plasticstyle=flat&logo=node.js)![](https://img.shields.io/badge/Tools-Git-yellow?style=plasticstyle=flat&logo=git)
 ![](https://img.shields.io/badge/Code-React-yellow?style=plasticstyle=flat&logo=react) ![](https://img.shields.io/badge/Code-Dart-yellow?style=plasticstyle=flat&logo=dart)  ![](https://img.shields.io/badge/Tools-PostgresSQL-yellow?style=plasticstyle=flat&logo=postgresql)   ![](https://img.shields.io/badge/Tools-GraphQl-yellow?style=plasticstyle=flat&logo=graphql) 

### **Solo MVPs:**

 - #### [ CRUD API GraphQL](https://gitlab.com/alolayan.nada/api-graphql)
    - [ ] implementation postgresql

 - - - -

 - #### [Animal in need locator](https://gitlab.com/alolayan.nada/cats-in-need)
    - [ ] allow users to share the location of animals in need they might find anywhere and place pins on google maps. Users can also search for animals in need
    - [ ] Implemented registration and authentication using JWT
    - [ ] Solo MVP Project Demo: https://animalsinneedlocator.herokuapp.com/
    - [ ] Utilized Google maps and Google places to find and store animal locations
    - [ ] React for front End
    - [ ] Sequelize as ORM
    - [ ] GraphQL for API calls
    __________________________________________________________________________
    
    ![](https://i.postimg.cc/npqGzXFg/Screen-Shot-2022-05-10-at-10-49-37.png)
    ![](https://i.postimg.cc/tgsVySk1/Screen-Shot-2022-05-10-at-10-49-52.png)

 - - - -

 - #### [Product Checker](https://gitlab.com/alolayan.nada/product-checker)
    - [ ] Flutter for cross platform mobile development (Android/iOS)
    - [ ] extract product information by scanning products using the Open Food Facts API.

 - - - -

- #### [Save Seals](https://gitlab.com/alolayan.nada/save-seals)
    - [ ] This project is dedicated to raise awareness of the brutalities inflicted on seals by humans
and to connect those who are interested together:
    - [ ] Solo MVP Project Demo: https://seals-help.herokuapp.com/
    - [ ] Used React for front End/ TypeScript, Node.js and Express.js for the backend
    - [ ] Used chakra UI library for user interface
    - [ ] Used Mongo Db for database and JWT for authentication




   ![]( https://i.postimg.cc/LHQz7fDY/Screen-Shot-2022-05-10-at-10-50-29.png)
   


 - - - -

 - #### [Vintage Store](https://gitlab.com/alolayan.nada/vintage-store-nes)
    - [ ]  An online gaming marketplace that allows vintage game sellers to sell retro games:
    - [ ] Solo/team MVP Project Demo: https://vintage-store-nes.herokuapp.com/
    - [ ] Implemented registration and authentication using JWT/authorization using refresh and access token
    - [ ] Integrated Stripe checkout for payment with authentication
    - [ ] Applied Knex as query builder for PostgreSQL and Axios for http requests
    - [ ] Created backend and front end using: React, Node.js Express

    
    
      ![]( https://i.postimg.cc/pd35dfcs/Screen-Shot-2022-05-10-at-10-51-25.png)

      ![]( https://i.postimg.cc/rwntBX1X/Screen-Shot-2022-05-10-at-10-52-01.png)
 - - - -

 - #### Pekify
    An app that allows users to peek into other user’s music around them or based on distance, my role included:
    - [ ] Team Project: https://master.d3koc4k87o35ht.amplifyapp.com/
    - [ ] Created the basic backend and front end using: React, Node.js Express and Typescript
    - [ ] Implemented basic authentication using JWT
    - [ ] Implemented Commenting feature using WebSocket technology
    - [ ] Organized tasks, presentations and provided support (technical and moral)



